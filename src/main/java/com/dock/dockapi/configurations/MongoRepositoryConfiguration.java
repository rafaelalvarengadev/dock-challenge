package com.dock.dockapi.configurations;

import com.dock.dockapi.user.models.User;
import com.dock.dockapi.user.repositories.UserCustomRepositoryImpl;
import com.dock.dockapi.user.repositories.UserRepository;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.RequiredArgsConstructor;
import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactoryBean;

@Configuration
@RequiredArgsConstructor
@EnableMongoRepositories(basePackages = "com.dock.dockapi")
public class MongoRepositoryConfiguration {

    @Value("${spring.data.mongodb.uri:}")
    private String uri;

    @Value("${spring.data.mongodb.database}")
    private String database;


    @Bean
    @Qualifier("userRepository")
    public UserRepository userRepository(final @Qualifier("primaryMongoTemplate") MongoTemplate template) {

        MongoRepositoryFactoryBean<UserRepository, User, String> factory =
                new MongoRepositoryFactoryBean<>(UserRepository.class);

        factory.setCustomImplementation(new UserCustomRepositoryImpl(template));
        factory.setMongoOperations(template);
        factory.afterPropertiesSet();

        return factory.getObject();
    }


    @Bean
    @Primary
    @Qualifier("primaryMongoTemplate")
    public MongoTemplate primaryMongoTemplate() {
        return new MongoTemplate(primaryFactory(this.uri, this.database));
    }

    @Bean
    @Primary
    public MongoDatabaseFactory primaryFactory(final String uri, final String database) {
        return createMongoDbFactory(uri, database);
    }

    private SimpleMongoClientDatabaseFactory createMongoDbFactory(final String uri, final String database) {
        return new SimpleMongoClientDatabaseFactory(createMongoClient(uri), database);
    }

    private MongoClient createMongoClient(final String clusterUrl) {
        return MongoClients.create(buildMongoClientSettings(clusterUrl));
    }

    private MongoClientSettings buildMongoClientSettings(final String clusterUrl) {
        return MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(clusterUrl))
                .codecRegistry(codecRegistries())
                .build();
    }

    private CodecRegistry codecRegistries() {
        return CodecRegistries.fromRegistries(
                CodecRegistries.fromProviders(new UuidCodecProvider(UuidRepresentation.STANDARD)),
                MongoClientSettings.getDefaultCodecRegistry());
    }
}

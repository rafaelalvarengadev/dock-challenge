package com.dock.dockapi.exceptionHandler.representations;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorMessageResponse {

    private String message;
}

package com.dock.dockapi.user.controllers;

import com.dock.dockapi.user.representations.response.UserResponse;
import com.dock.dockapi.user.representations.request.UserCreateRequest;
import com.dock.dockapi.exceptionHandler.representations.ErrorMessageResponse;
import io.swagger.annotations.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;

@Api(tags = "User", value = "user", description = "Api Rest")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorMessageResponse.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMessageResponse.class)
})
@RequestMapping(value = "/users", produces = {"application/json;charset=UTF-8"})
public interface UserController {

    @ApiOperation(value = "Responsavel pela persistencia dos dados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "oK"),
            @ApiResponse(code = 400, message = "bad request"),
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping
    ResponseEntity<?> save(
            @Valid @RequestBody UserCreateRequest request,
            BindingResult bindingResult
    );


    @ApiOperation(
            value = "Retorna uma lista de Usuarios, através de filtros")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "page",
                    defaultValue = "0",
                    dataType = "integer",
                    paramType = "query",
                    value = "Numero da pagina da busca"),
            @ApiImplicitParam(
                    name = "size",
                    defaultValue = "20",
                    dataType = "integer",
                    paramType = "query",
                    value = "Tamanho da pagina da busca"),
            @ApiImplicitParam(
                    name = "direction",
                    allowableValues = "ASC,DESC",
                    defaultValue = "DESC",
                    dataType = "string",
                    paramType = "query",
                    value = "Direção da ordenação da busca")
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    ResponseEntity<PagedModel<EntityModel<UserResponse>>> list(
            @ApiParam(value = "Data minima", required = false)
            @RequestParam(value = "from", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") final String paramFrom,

            @ApiParam(value = "Data máxima", required = false)
            @RequestParam(value = "to", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") final String paramTo,

            @ApiParam(value = "User Reference Id")
            @RequestParam(required = false) final String id,

            @ApiParam(value = "Data")
            @RequestParam(required = false) final String data,

            @ApiParam(value = "Name of User")
            @RequestParam(required = false) final String name,

            @ApiParam(value = "State")
            @RequestParam(required = false) final String state,

            @ApiParam(value = "Value")
            @RequestParam(required = false) final Long value,

            @ApiParam(value = "low")
            @RequestParam(required = false) final String low,

            @ApiIgnore("Ignored because swagger ui shows the wrong params, "
                    + "instead they are explained in the implicit params")
            @PageableDefault(
                    page = 0,
                    size = 20,
                    sort = "id",
                    direction = Sort.Direction.DESC) final Pageable pageable,
            final PagedResourcesAssembler<UserResponse> pagedResourcesAssembler,
            @ApiIgnore final Principal principal
    );

}

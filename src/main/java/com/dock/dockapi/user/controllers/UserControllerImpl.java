package com.dock.dockapi.user.controllers;

import com.dock.dockapi.user.representations.request.UserFilterRequest;
import com.dock.dockapi.user.representations.response.UserResponse;
import com.dock.dockapi.user.representations.request.UserCreateRequest;
import com.dock.dockapi.user.services.UserService;
import com.dock.dockapi.exceptionHandler.representations.ErrorMessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Slf4j
@RestController
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Autowired
    public UserControllerImpl(final UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<?> save(UserCreateRequest request, BindingResult bindingResult) {

        try {
            log.info("Creating new User, payload: " + request);
            var response = userService.bindToResponse(userService.create(request));

            return new ResponseEntity<>(response, HttpStatus.CREATED);

        } catch (RuntimeException ex) {
            return new ResponseEntity<>(new ErrorMessageResponse(ex.getMessage()), HttpStatus.UNPROCESSABLE_ENTITY);

        }
    }

    @Override
    public ResponseEntity<PagedModel<EntityModel<UserResponse>>> list(
            String paramFrom,
            String paramTo,
            String id,
            String data,
            String name,
            String state,
            Long value,
            String low,
            Pageable pageable,
            PagedResourcesAssembler<UserResponse> pagedResourcesAssembler,
            Principal principal
    ) {

        try {
            log.info("Listing Users");

            var request = UserFilterRequest.builder()
                    .id(id).data(data).name(name).state(state).value(value).low(low)
                    .build()
                    .withFrom(paramFrom).withTo(paramTo);

            var userResponsePage = userService.list(request, pageable).map(userService::bindToResponse);

            var link = linkTo(methodOn(UserControllerImpl.class)
                    .list(paramFrom,
                            paramTo,
                            id,
                            data,
                            name,
                            state,
                            value,
                            low,
                            pageable,
                            pagedResourcesAssembler,
                            principal))
                    .withSelfRel();
            return new ResponseEntity<>(pagedResourcesAssembler.toModel(userResponsePage, link), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

}

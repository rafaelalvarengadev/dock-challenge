package com.dock.dockapi.user.repositories;

import com.dock.dockapi.user.representations.queries.UserFilters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;


@NoRepositoryBean
public interface UserCustomRepository<T, ID> extends Repository<T, ID> {

    Page<T> findAllWithFilters(UserFilters filters, Pageable pageable);
}

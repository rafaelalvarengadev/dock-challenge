package com.dock.dockapi.user.repositories;

import com.dock.dockapi.user.models.User;
import com.dock.dockapi.user.representations.queries.UserFilters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Slf4j
@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository<User, String> {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public UserCustomRepositoryImpl(@Qualifier("primaryMongoTemplate") MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Page<User> findAllWithFilters(final UserFilters filters, final Pageable pageable) {
        var criterias = new ArrayList<>();

        if (filters.getFrom() != null) {
            criterias.add(where("createdAt").gte(filters.getFrom()));
        }
        if (filters.getTo() != null) {
            criterias.add(where("createdAt").lt(filters.getTo()));
        }
        if (filters.getData() != null) {
            criterias.add(where("data").is(filters.getData()));
        }

        if (filters.getName()!= null) {
            criterias.add(where("name").is(filters.getName()));
        }

        if (filters.getState() != null) {
            criterias.add(where("state").is(filters.getState()));
        }

        if (filters.getBravery() != null) {
            criterias.add(where("bravery").is(filters.getBravery()));
        }

        if (filters.getValue() != null) {
            criterias.add(where("value").is(filters.getValue()));
        }

        if (filters.getLow() != null) {
            criterias.add(where("low").is(filters.getLow()));
        }
        var query = new Query(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()])))
                .with(pageable);

        List<User> users;
        try {
           users  = mongoTemplate.find(query, User.class);
        }catch (Exception ex){
            log.error(ex.getMessage());
            throw ex;
        }
        
        return PageableExecutionUtils.getPage(
                users,
                pageable,
                () -> mongoTemplate.count((Query.of(query).limit(-1).skip(-1)), User.class));
    }

}

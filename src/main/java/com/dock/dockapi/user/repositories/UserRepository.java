package com.dock.dockapi.user.repositories;

import com.dock.dockapi.user.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface UserRepository extends MongoRepository<User, String>, UserCustomRepository<User, String> {
}

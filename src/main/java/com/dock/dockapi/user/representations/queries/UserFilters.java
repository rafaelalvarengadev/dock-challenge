package com.dock.dockapi.user.representations.queries;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
public class UserFilters {

    private String data;

    private String id;

    private String name;

    private String state;

    private Long value;

    private String bravery;

    private Boolean low;

    private Instant from;

    private Instant to;

    public boolean hasFilter(){
        return !Stream.of(data, id, name, state, value, bravery, low, from, to).allMatch(Objects::isNull);

    }
}

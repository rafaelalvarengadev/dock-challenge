package com.dock.dockapi.user.representations.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
public class UserCreateRequest {

    private String data;

    private String id;

    private String name;

    private String state;

    private String bravery;

    private String low;
}

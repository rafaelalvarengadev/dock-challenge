package com.dock.dockapi.user.representations.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Data
@Builder
@AllArgsConstructor
public class UserFilterRequest {

    @ApiModelProperty(value = "Data")
    private String data;

    @ApiModelProperty(value = "User Identification")
    private String id;

    @ApiModelProperty(value = "User Name")
    private String name;

    @ApiModelProperty(value = "State")
    private String state;

    @ApiModelProperty(value = "Value")
    private Long value;

    @ApiModelProperty(value = "Bravery")
    private String bravery;

    @ApiModelProperty(value = "low")
    private String low;

    @ApiModelProperty(value = "Initial Date")
    private Instant from;

    @ApiModelProperty(value = "Finally Date")
    private Instant to;


    public UserFilterRequest withFrom(final String from) {
        if (from != null){
            this.setFrom(convert(from));
        }
        return this;
    }

    public UserFilterRequest withTo(final String to) {
        if (to != null) {
            this.setTo(convert(to));
        }
        return this;
    }


    public static Instant convert(final String source) {

        var formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        var time = LocalDate.parse(source, formatter);

        return time.atStartOfDay(ZoneId.systemDefault()).toInstant();
    }
}

package com.dock.dockapi.user.services;

import com.dock.dockapi.user.models.User;
import com.dock.dockapi.user.representations.request.UserFilterRequest;
import com.dock.dockapi.user.representations.request.UserCreateRequest;
import com.dock.dockapi.user.representations.response.UserResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    Page<User> list(UserFilterRequest request, Pageable pageable);

    User create(UserCreateRequest request);

    UserResponse bindToResponse(User user);
}

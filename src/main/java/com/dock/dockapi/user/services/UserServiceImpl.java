package com.dock.dockapi.user.services;

import com.dock.dockapi.user.controllers.UserController;
import com.dock.dockapi.user.models.User;
import com.dock.dockapi.user.repositories.UserRepository;
import com.dock.dockapi.user.representations.request.UserCreateRequest;
import com.dock.dockapi.user.representations.request.UserFilterRequest;
import com.dock.dockapi.user.representations.queries.UserFilters;
import com.dock.dockapi.user.representations.response.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Page<User> list(final UserFilterRequest requestFilters, final Pageable pageable) {
        var filters = new UserFilters();
        BeanUtils.copyProperties(requestFilters, filters);

        if (filters.hasFilter()){
            return list(filters, pageable);
        }

        return list(pageable);
    }

    private Page<User> list(final Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    private Page<User> list(final UserFilters filters, final Pageable pageable) {
        return userRepository.findAllWithFilters(filters, pageable);
    }


    @Override
    public User create(final UserCreateRequest request) {
        log.info("creating user [{}] ", request);

        var user = User.builder()
                .withUserReferenceId(request.getId())
                .withData(request.getData())
                .withName(request.getName())
                .withState(request.getState())
                .withLow(request.getLow())
                .withBravery(request.getBravery())
                .withCreatedAt(Instant.now())
                .withUpdatedAt(Instant.now())
                .build();

        return userRepository.save(user);
    }


    public UserResponse bindToResponse(final User user) {
        var response = UserResponse.builder().build();
        BeanUtils.copyProperties(user, response);
        response.add(WebMvcLinkBuilder
                .linkTo(UserController.class)
                .slash(user.getId())
                .withSelfRel());
        return response;
    }
}

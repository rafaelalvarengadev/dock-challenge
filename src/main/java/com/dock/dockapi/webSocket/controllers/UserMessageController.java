package com.dock.dockapi.webSocket.controllers;

import com.dock.dockapi.user.models.User;
import com.dock.dockapi.webSocket.representations.Message;
import com.dock.dockapi.webSocket.representations.UserMessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Controller
public class UserMessageController {

    @MessageMapping("/user")
    @SendTo("/topic/users")
    public UserMessageResponse user(Message message) {
        log.info(message.getMessage());

        var user = UserMessageResponse.builder()
                .user(User.builder().withName(message.getMessage()).build())
                .build();
        var teste = splitUserMessage(message.getMessage());

        return user;
    }

    private String splitUserMessage(String message){
        var user = User.builder().build();
        message = message.replaceAll("\s", "");

        final String regex = "(([\\d]{3,})\\|(\\w+))";
        final String subst = "";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(message);
//        var result = matcher.results().filter(w -> w.group(2).contains("001")).findFirst().get();

        log.info(matcher.group(1));
        if (matcher.group(2).equals("001")){
            user.setData(matcher.group(3));
        }

        return "";
    }
}

package com.dock.dockapi.webSocket.representations;

import com.dock.dockapi.user.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserMessageResponse {

    private User user;
}

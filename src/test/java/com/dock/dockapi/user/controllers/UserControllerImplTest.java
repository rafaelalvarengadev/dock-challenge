package com.dock.dockapi.user.controllers;

import com.dock.dockapi.user.models.User;
import com.dock.dockapi.user.representations.request.UserFilterRequest;
import com.dock.dockapi.user.representations.response.UserResponse;
import com.dock.dockapi.user.services.UserService;
import com.dock.dockapi.user.services.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserControllerImpl.class)
@AutoConfigureMockMvc
class UserControllerImplTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private UserService userService;

    private MockMvc mockMvc;


    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @DisplayName("must return 200 for requisition without filter")
    void listSuccessWithoutFilters() throws Exception {

        List<User> users = new ArrayList<>();
        Instant now = Instant.now();
        var id = UUID.randomUUID().toString();
        var referenceId = "50186";
        var name = "Raul Seixas";
        var data = "10032019";
        var low = "SRF001";



        var userResponse = UserResponse.builder()
                .withId(id)
                .withUserReferenceId("50186")
                .withName(name)
                .withBravery("0000015000")
                .withData(data)
                .withLow(low)
                .withCreatedAt(now)
                .build().add(WebMvcLinkBuilder
                .linkTo(UserController.class)
                .slash(id)
                .withSelfRel());


        Page<User> userPage = new PageImpl<>(users);

        when(userService.bindToResponse(any())).thenReturn(userResponse);
        when(userService.list(any(UserFilterRequest.class), any(Pageable.class))).thenReturn(userPage);

        mockMvc.perform(
                        get("/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("must return 200 for requisition with  any filter")
    void listSuccessWithFilters() throws Exception {

        List<User> users = new ArrayList<>();
        Instant now = Instant.now();
        var id = UUID.randomUUID().toString();
        var referenceId = "50186";
        var name = "Raul Seixas";
        var data = "10032019";
        var low = "SRF001";


        var userResponse = UserResponse.builder()
                .withId(id)
                .withUserReferenceId("50186")
                .withName(name)
                .withBravery("0000015000")
                .withData(data)
                .withLow(low)
                .withCreatedAt(now)
                .build().add(WebMvcLinkBuilder
                        .linkTo(UserController.class)
                        .slash(id)
                        .withSelfRel());


        Page<User> userPage = new PageImpl<>(users);

        when(userService.bindToResponse(any())).thenReturn(userResponse);
        when(userService.list(any(UserFilterRequest.class), any(Pageable.class))).thenReturn(userPage);

        mockMvc.perform(
                        get("/users?low=SRF001").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}